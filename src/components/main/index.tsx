import styles from "./style.module.css";
interface IMainProps {
  children: React.ReactNode;
}
function Main({ children }: IMainProps) {
  return (
    <main>
      <div className={styles["current-page-container"]}>
        <img className={styles["home-icon"]} src={require("./assets/icons/home-icon.png")} />
        <img
          className={styles["arrow-icon"]}
          src={require("./assets/icons/arrow-right-icon.png")}
        />
        <span>INSTITUCIONAL</span>
      </div>
      {children}
    </main>
  );
}

export { Main };
