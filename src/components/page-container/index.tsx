import styles from "./style.module.css";

interface IContainerProps {
  children: React.ReactNode;
}

function Container({ children }: IContainerProps) {
  return <div className={styles["container"]}>{children}</div>;
}

export { Container };
