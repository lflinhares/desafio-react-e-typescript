import styles from "./style.module.css";

import whatsappIcon from "./assets/png/whatsapp-icon.png";
import arrow from "./assets/png/arrow.png";
function FixedButtons() {
  function handleClick() {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }

  return (
    <div className={styles["container"]}>
      <a href="/" target="_blank">
        <img src={whatsappIcon} alt="whatsapp" />
      </a>
      <button className={styles["scroll-up-button"]} onClick={handleClick}>
        <img className={styles["scroll-up-img"]} src={arrow} alt="seta para cima" />
      </button>
    </div>
  );
}

export { FixedButtons };
