import { useEffect, useRef, useState } from "react";
import styles from "./style.module.css";

import mobileMenuIcon from "./assets/png/mobile-menu-icon.png";
import logoM3 from "./assets/png/logo-m3.png";
import searchIcon from "./assets/png/search-icon.png";
import cart from "./assets/png/cart.png";

function Header() {
  const [mobileMenu, setMobileMenu] = useState(false);
  const mobileMenuRef = useRef<HTMLDivElement>(null);
  const mobileMenuIconRef = useRef<HTMLImageElement>(null);

  function handleMobileMenuIconClick() {
    setMobileMenu(!mobileMenu);
  }

  function closeMobileMenu(e: MouseEvent) {
    if (
      mobileMenuRef.current &&
      !mobileMenuRef.current?.contains(e.target as Node) &&
      mobileMenuIconRef.current &&
      !mobileMenuIconRef.current?.contains(e.target as Node)
    ) {
      setMobileMenu(false);
    }
  }

  useEffect(() => {
    document.addEventListener("click", closeMobileMenu);
    return () => {
      document.removeEventListener("click", closeMobileMenu);
    };
  });

  return (
    <header>
      <div className={styles["upper-header"]}>
        <img
          alt="menu-icon"
          className={styles["mobile-menu-icon"]}
          src={mobileMenuIcon}
          onClick={handleMobileMenuIconClick}
          ref={mobileMenuIconRef}
        />

        <div className={styles["logo-container"]}>
          <img alt="logo" src={logoM3} />
        </div>

        <div className={styles["search-container"]}>
          <input type="text" placeholder="Buscar..." />

          <button>
            <img alt="busca" src={searchIcon} />
          </button>
        </div>

        <div className={styles["login-cart-container"]}>
          <a href="/">ENTRAR</a>
          <img alt="carrinho" src={cart} />
        </div>
      </div>

      <div
        className={
          styles["bottom-header"] +
          " " +
          (mobileMenu ? styles["bottom-header-open"] : styles["bottom-header-closed"])
        }
        ref={mobileMenuRef}
      >
        <nav className={styles["menu"]}>
          <ul>
            <li className={styles["sign-in-mobile"]}>
              <a href="/" className={styles["menu-item"]}>
                ENTRAR
              </a>
            </li>
            <li>
              <a href="/" className={styles["menu-item"]}>
                CURSOS
              </a>
            </li>
            <li>
              <a href="/" className={styles["menu-item"]}>
                SAIBA MAIS
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
}

export { Header };
