import styles from "./style.module.css";
import { useState } from "react";

import plusIcon from "./assets/png/plus-icon.png";

interface IMiddleFooterUlProps {
  obj: { title: string; arr: string[]; link: boolean };
}

function MiddleFooterUl({ obj }: IMiddleFooterUlProps) {
  const [dropDown, setDropDown] = useState(false);

  function handleMobileMiddleFooterDropDown() {
    setDropDown(!dropDown);
  }

  return (
    <ul
      className={
        styles["middle-footer-ul"] +
        " " +
        (dropDown ? styles["middle-footer-opened"] : styles["middle-footer-closed"])
      }
    >
      <li className={styles["middle-footer-li"]} onClick={handleMobileMiddleFooterDropDown}>
        <h3>{obj.title}</h3>
        <img src={plusIcon} alt="Mostrar mais" />
      </li>
      {obj.link === true ? (
        obj.arr.map((item, index) => (
          <li className={styles["middle-footer-li"]} key={index}>
            <a href="/">{item}</a>
          </li>
        ))
      ) : (
        <>
          <li className={styles["middle-footer-li"]}>
            <h4>{obj.arr[0]}</h4>
          </li>
          <li className={styles["middle-footer-li"]}>{obj.arr[1]}</li>
          <li className={styles["middle-footer-li"]}>
            <h4>{obj.arr[2]}</h4>
          </li>
          <li className={styles["middle-footer-li"]}>{obj.arr[3]}</li>
        </>
      )}
    </ul>
  );
}

export { MiddleFooterUl };
