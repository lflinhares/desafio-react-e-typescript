import styles from "./style.module.css";
import { Formik, Form, Field, ErrorMessage, FormikHelpers } from "formik";
import institucionalSchema from "../../schemas/institucional-schema";

interface IFormikValues {
  name: string;
  email: string;
  cpf: string;
  birthday: string;
  phone: string;
  instagram: string;
  acceptTerms: boolean;
}
const initialValues = {
  name: "",
  email: "",
  cpf: "",
  birthday: "",
  phone: "",
  instagram: "",
  acceptTerms: false,
};

function InstitucionalForm() {
  function handleSubmit(values: IFormikValues, actions: FormikHelpers<IFormikValues>) {
    actions.resetForm();
  }

  return (
    <>
      <Formik
        onSubmit={handleSubmit}
        initialValues={initialValues}
        validationSchema={institucionalSchema}
      >
        <Form className={styles["form"]}>
          <h2>PREENCHA O FORMULÁRIO</h2>
          <div className={styles["field-container"]}>
            <label className={styles["form-label"]} htmlFor="name">
              Nome
            </label>
            <ErrorMessage component="span" name="name" className={styles["form-error"]} />
            <Field
              className={styles["form-input"]}
              name="name"
              id="name"
              placeholder="Seu nome completo"
            />
          </div>
          <div className={styles["field-container"]}>
            <label className={styles["form-label"]} htmlFor="email">
              E-mail
            </label>
            <ErrorMessage component="span" name="email" className={styles["form-error"]} />
            <Field
              className={styles["form-input"]}
              name="email"
              id="email"
              placeholder="Seu e-mail"
            />
          </div>
          <div className={styles["field-container"]}>
            <label className={styles["form-label"]} htmlFor="cpf">
              CPF
            </label>
            <ErrorMessage component="span" name="cpf" className={styles["form-error"]} />
            <Field
              className={styles["form-input"]}
              name="cpf"
              id="cpf"
              placeholder="000 000 000 00"
            />
          </div>
          <div className={styles["field-container"]}>
            <label className={styles["form-label"]} htmlFor="birthday">
              Data de Nascimento:
            </label>
            <ErrorMessage component="span" name="birthday" className={styles["form-error"]} />
            <Field
              className={styles["form-input"]}
              name="birthday"
              id="birthday"
              placeholder="00 . 00 . 0000"
            />
          </div>
          <div className={styles["field-container"]}>
            <label className={styles["form-label"]} htmlFor="phone">
              Telefone:
            </label>
            <ErrorMessage component="span" name="phone" className={styles["form-error"]} />
            <Field
              className={styles["form-input"]}
              name="phone"
              id="phone"
              placeholder="(00) 00000-0000"
            />
          </div>
          <div className={styles["field-container"]}>
            <label className={styles["form-label"]} htmlFor="instagram">
              Instagram
            </label>
            <ErrorMessage component="span" name="instagram" className={styles["form-error"]} />
            <Field
              className={styles["form-input"]}
              name="instagram"
              id="instagram"
              placeholder="@seuuser"
            />
          </div>
          <div className={styles["field-container"]}>
            <label className={styles["accept-terms"]} htmlFor="acceptTerms">
              <a>
                <i>*</i>Declaro que li e aceito
              </a>
              <Field type="checkbox" name="acceptTerms"></Field>
            </label>
            <ErrorMessage
              component="span"
              name="acceptTerms"
              className={styles["accept-terms-error"]}
            />
          </div>
          <button type="submit" className={styles["form-button"]}>
            CADASTRE-SE
          </button>
        </Form>
      </Formik>
    </>
  );
}

export { InstitucionalForm };
