import React, { ChangeEventHandler } from "react";
import styles from "./style.module.css";

interface IInstitucionalProps {
  data: {
    title: string;
  };
  onClick: React.MouseEventHandler<HTMLLabelElement>;
}

function InstitucionalLi({ data, onClick }: IInstitucionalProps) {
  return (
    <li className={styles["infos-li"]}>
      <input
        className={styles["radio"]}
        type="radio"
        id={data.title}
        name="radio"
        defaultChecked={data.title === "Sobre" ? true : false}
      />
      <label
        id={data.title}
        onClick={onClick}
        className={styles["infos-li-label"]}
        htmlFor={data.title}
      >
        <div className={styles["li-title"]}>{data.title}</div>
      </label>
    </li>
  );
}

export { InstitucionalLi };
