import styles from "./style.module.css";
import { Formik, Form, Field, FormikHelpers } from "formik";
import newsletterSchema from "../../schemas/newsletter-schema";
import { MiddleFooterUl } from "../middle-footer-menu-ul";

import facebookIcon from "./assets/png/facebook-icon.png";
import instagramIcon from "./assets/png/instagram-icon.png";
import linkedinIcon from "./assets/png/linkedin-icon.png";
import twitterIcon from "./assets/png/twitter-icon.png";
import youtubeIcon from "./assets/png/youtube-icon.png";

import paymentOptionsImage from "./assets/svg/payment-options.svg";

import vtex from "./assets/svg/vtex.svg";
import m3 from "./assets/svg/m3.svg";

interface IFormikValues {
  email: string;
}
const initialValues = {
  email: "",
};

interface IMiddleFooterUl {
  title: string;
  arr: string[];
  link: boolean;
}

const INSTITUCIONAL: IMiddleFooterUl = {
  title: "Institucional",
  arr: ["Quem Somos", "Política de Privacidade", "Segurança", "Seja um Revendedor"],
  link: true,
};

const DUVIDAS: IMiddleFooterUl = {
  title: "Dúvidas",
  arr: ["Entrega", "Pagamento", "Trocas e Devoluções", "Dúvidas Frequentes"],
  link: true,
};

const faleConosco: IMiddleFooterUl = {
  title: "Fale Conosco",
  arr: ["Atendimento ao consumidor", "(11) 4159 9504", "Atendimento online", "(11) 99433-8825"],
  link: false,
};

const SOCIAL = [
  { img: facebookIcon, alt: "facebook", link: "https://www.instagram.com/m3.ecommerce/" },
  { img: instagramIcon, alt: "instagram", link: "https://www.facebook.com/digitalm3/" },
  { img: linkedinIcon, alt: "linkedin", link: "https://www.linkedin.com/company/m3ecommerce" },
  { img: twitterIcon, alt: "twitter", link: "/" },
  {
    img: youtubeIcon,
    alt: "youtube",
    link: "https://www.youtube.com/channel/UCSMoUkouJCf_LUYfFiIK2yw",
  },
];

function Footer() {
  function handleSubmit(values: IFormikValues, actions: FormikHelpers<IFormikValues>) {
    actions.resetForm();
  }

  return (
    <footer>
      <div className={styles["upper-footer"]}>
        <div className={styles["form-container"]}>
          <h3>ASSINE NOSSA NEWSLETTER</h3>
          <Formik
            onSubmit={handleSubmit}
            initialValues={initialValues}
            validationSchema={newsletterSchema}
          >
            <Form>
              <Field
                className={styles["email-input"]}
                name="email"
                id="email"
                placeholder="E-mail"
              />

              <button type="submit" className={styles["submit-email-button"]}>
                ENVIAR
              </button>
            </Form>
          </Formik>
        </div>
      </div>
      <div className={styles["middle-footer"]}>
        <MiddleFooterUl obj={INSTITUCIONAL}></MiddleFooterUl>
        <MiddleFooterUl obj={DUVIDAS}></MiddleFooterUl>
        <MiddleFooterUl obj={faleConosco}></MiddleFooterUl>

        <div className={styles["social"]}>
          {SOCIAL.map((item, index) => (
            <a key={index} href={item.link} target="_blank" rel="noreferrer">
              <img alt={item.alt} src={item.img} />
            </a>
          ))}

          <a className={styles["website-link"]} target="_blank" href="/">
            www.loremipsum.com
          </a>
        </div>
      </div>

      <div className={styles["bottom-footer"]}>
        <span className={styles["bottom-footer-text"]}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
        </span>

        <img
          src={paymentOptionsImage}
          className={styles["payment-img"]}
          alt="Opções de pagamento"
        />

        <div className={styles["credits"]}>
          <div className={styles["credit-container"]}>
            <span className={styles["credit-text"]}> Powered by </span>
            <a
              className={styles["credit-link"]}
              href="https://vtex.com/br-pt/"
              target="_blank"
              rel="noreferrer"
            >
              <img src={vtex} className={styles["vtex-img"]} alt="Vtex Logo" />
            </a>
          </div>
          <div className={styles["credit-container"]}>
            <span className={styles["credit-text"]}> Developed by </span>
            <a
              className={styles["credit-link"]}
              href="https://m3ecommerce.com/"
              target="_blank"
              rel="noreferrer"
            >
              <img src={m3} className={styles["m3-img"]} alt="m3 Logo" />
            </a>
          </div>
        </div>
      </div>
    </footer>
  );
}

export { Footer };
