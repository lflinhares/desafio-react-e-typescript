import { Header } from "../components/header";
import { Footer } from "../components/footer";
import { Container } from "../components/page-container";
import { Main } from "../components/main";
import { Institucional } from "../components/institucional";
import { FixedButtons } from "../components/fixed-buttons";
function Home() {
  return (
    <Container>
      <Header />
      <Main>
        <Institucional />
      </Main>
      <FixedButtons />
      <Footer />
    </Container>
  );
}

export { Home };
