import * as Yup from "yup";
import { parse, isDate } from "date-fns";
import { validateCPF, validatePhone } from "validations-br";

const today = new Date();
const dateMax = today.getDate() + "." + (today.getMonth() + 1) + "." + (today.getFullYear() - 12);

function parseDateString(value: Date, originalValue: string) {
  const parsedDate = isDate(originalValue)
    ? originalValue
    : parse(originalValue, "dd.MM.yyyy", new Date());

  return parsedDate;
}

function validateInstagram(insta: string | undefined) {
  if (insta && insta.length > 2 && insta[0] === "@") return true;
  else return false;
}

export default Yup.object().shape({
  name: Yup.string()
    .required("* Campo obrigatório.")
    .test("name-length", "* Digite seu nome completo.", (name) => {
      if (name && name.length > 10) return true;
      else return false;
    }),

  email: Yup.string().required("* Campo obrigatório.").email("* Email inválido."),

  cpf: Yup.string()
    .required("* Campo obrigatório.")
    .test("test-cpf", "* CPF inválido.", (cpf) => {
      if (cpf) {
        return validateCPF(cpf);
      } else {
        return false;
      }
    }),

  birthday: Yup.date()
    .required("* Campo obrigatório")
    .typeError("* Data inválida.")
    .transform(parseDateString)
    .max(dateMax, "* Data inválida."),

  phone: Yup.string()
    .required("* Campo obrigatório.")
    .test("test-phone", "* Telefone inválido.", (phone) => {
      if (phone) {
        return validatePhone(phone);
      } else {
        return false;
      }
    }),

  instagram: Yup.string()
    .required("* Campo obrigatório.")
    .test("test-insta", "* Instagram inválido.", (insta) => {
      return validateInstagram(insta);
    }),

  acceptTerms: Yup.boolean().oneOf([true], "* Aceite os termos para continuar."),
});
